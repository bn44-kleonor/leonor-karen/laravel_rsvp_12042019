@extends("layouts.app")
@section("content")
<div class="row">
	<div class="col">
		<div class="display-4">{{ $title }}</div>
	</div>
</div>

<form method="POST" action="/owners">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<!-- Name --> 
					<div class="form-group">
						<label for="first_name">
							First Name:
						</label>
						<input type="text" name="first_name" id="first_name" class="form-control">
					</div>
					<div class="form-group">
						<label for="last_name">
							Last Name:
						</label>
						<input type="text" name="last_name" id="last_name" class="form-control">
					</div>
					<!-- Email -->
					<div class="form-group">
						<label for="email">
							Email:
						</label>
						<input type="text" name="email" id="email" class="form-control">
					</div>
					<!-- Contact Number -->
					<div class="form-group">
						<label for="contact_number">
							Contact No.:
						</label>
						<input type="text" name="contact_number" id="contact_number" class="form-control">
					</div>
					<!-- Response -->
					<div class="form-group">
						<label for="contact_number">
							<!-- Response -->
						</label>
						<input type="hidden" name="contact_number" id="contact_number" class="form-control" value="" disabled>
					</div>
					<!-- EVENT ID | hidden -->
					<div class="form-group">
						<label for="event_id">
							<!-- EVENT ID: -->
						</label>
						<input type="hidden" name="event_id" id="event_id" class="form-control" value="1">
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-warning">
						Create Invitee
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection