<!-- Create Events -->
@extends("layouts.app")
@section("content")

<p>test this 'ownerview - create'</p>

<div class="row">
	<div class="col">
		<div class="display-4">{{ $title }}</div>
	</div>
</div>

<form method="POST" action="/events">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<!-- Name --> 
					<div class="form-group">
						<label for="name">
							Name:
						</label>
						<input type="text" name="name" id="name" class="form-control">
					</div>
					<!-- Occassion -->
					<div class="form-group">
						<label for="name">
							Occasion:
						</label>
						<input type="text" name="occassion" id="occassion" class="form-control">
					</div>
					<!-- Place -->
					<div class="form-group">
						<label for="place">
							Place:
						</label>
						<input type="text" name="place" id="place" class="form-control">
					</div>
					<!-- Date -->
					<div class="form-group">
						<label for="date">
							Date:
						</label>
						<input type="text" name="date" id="date" class="form-control">
					</div>
					<!-- Owner id | hidden -->
					<div class="form-group">
						<label for="owner_id">
							<!-- Owner ID: -->
						</label>
						<input type="hidden" name="owner_id" id="owner_id" class="form-control" value="1">
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-warning">
						Create Event
					</button>
				</div>
			</div>
		</div>
	</div>
</form>

@endsection