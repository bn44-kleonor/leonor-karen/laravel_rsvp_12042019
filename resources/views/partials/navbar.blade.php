<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links --> 

                        <!-- If "LOGGED OUT" -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif

                        <!-- If "Logged in" -->
                        @else
                            <!-- If "ADMIN" -->
                            @if(Auth::check() && Auth::user()->role == 'admin')
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="/events">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/owners">Event_Owners</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/ownerview/2">Owner_View</a>
                                </li>
                            <!-- If "USER" -->
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="/ownerview/2">Owner_View</a>
                                </li>
                            @endif
                            
                            <!-- Search Bar -->
                            <form method= "POST" class="input-group input-group-sm my-2 my-lg-0 ml-5" id="search-form">
                                @csrf
                                <div class="mx-1">
                                    <i class="fas fa-search"></i>
                                </div>
                                <div>
                                    <input class="form-control mr-sm-2 " type="search" placeholder="Search Event" aria-label="Search" name="search" id="search">
                                </div>
                            </form>

                            <!-- Display button "LOG OUT"  -->
                            <li class="nav-item" href="/">
                                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Hi,{{ Auth::user()->username }}
                                </a>
                             </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>

                        @endguest
                    </ul>


                </div>
            </div>
</nav>