@extends("layouts.app")
@section("content")
	<!-- ownerS IN TABLE --> 
	<div class="row">
		<div class="col">
			<div class="display-4">{{ $title }}</div>
		</div>
	</div>
	<div class="row">
			<div class="col">
				<table class="table">
				  <thead class="thead-dark">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Name</th>
				      <th scope="col">Email</th>
				      <th scope="col">Address</th>
				      <th scope="col">No. of Events </th>
				      <th scope="col">Action</th>
				    </tr>
				  </thead>
				  <tbody>
					<?php $i = 1; ?>
				  	@foreach($owners as $owner)
				    <tr>
				      <th scope="row"><?php echo $i++; ?></th>
				      <td>{{ $owner->first_name . " " . $owner->last_name }}</td>
				      <td>{{ $owner->email }}</td>
				      <td>{{ $owner->address }}</td>
				      <td> # </td>
			          <td>
			            <div class="d-flex flex-row">
			                <!-- READ -->
			                <a class="btn btn-primary mr-1" href="/owners/{{ $owner->id }}/show">	
			                	View
			                </a>
			                <!-- UPDATE -->
			                <a class="btn btn-warning mr-1" href="/owners/{{ $owner->id }}/edit">
			                	Edit
			                </a>
							<!-------------- DELETE------------ -->
							<!-- DELETE button (easy version) -->
			                <!-- <form method="POST" action="/owners/{{ $owner->id }}">
			                	@csrf
			                	{{ method_field("DELETE") }}
			                	<button class="btn btn-danger mr-1">Delete</button>
			                </form> -->

							<!-- DELETE 2 (not easy (moderate version) - with modal) -->
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-danger btn-delete-owner" data-toggle="modal" data-target="#delete_owner_modal" data-id="{{$owner->id}}" data-name="{{$owner->first_name}}">
							  Delete
							</button>
							<!-- Modal -->
							<div class="modal fade" id="delete_owner_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        Do you want to delete <span id="delete_owner_name"></span> ?
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <form method="POST" id="delete_modal_form">
							        	@csrf
										{{ method_field("DELETE") }}
							        	<button class="btn btn-danger">Delete</button>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
							<!--------------END DELETE------------ -->
			            </div>
			          </td>
				    </tr>
				    @endforeach
				  </tbody>
				</table>
			</div>
	</div>
	<div class="row">
        <div class="col">
            <a class="btn btn-success" href="/owners/create">
            	Create Owner
            </a>
        </div>
    </div>
    <div>
    	<div class="col d-flex">
    		<div class="mx-auto">
    		</div>
    	</div>
    </div>
@endsection