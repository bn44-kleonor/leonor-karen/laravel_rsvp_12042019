<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'PageController@welcome');

Route::post('/search', "PageController@search");

Route::get("/ownerview/inviteelist", "PageController@inviteelist");

//unang pagrereferan upon logging in
Route::group(["middleware" => "auth"], function () {
	//for LOGGED IN
	// Route::get("/approval", "HomeController@approval")->name("approval");
	Route::get('/home', 'HomeController@index')->name('home');

	// Route::middleware(["approved"])->group(function(){
	// 	//for logged in and approved user
	// 	Route::get('/home', 'HomeController@index')->name('home');
	// });
	// Route::get("/confirmation/{id}", "PageController@confirmation");

	// ADMIN/USER - Individual OWNER - all about OwnerVIEW pages
	Route::post('/ownerview/{id}', 'OwnerviewController@store');	//store the crated new owner
	Route::get("/ownerview/{id}", "OwnerviewController@index");		//view owners list
	Route::delete("/ownerview/{id}", "OwnerviewController@destroy"); //soft delete owner
	Route::get("/ownerview/restore/{id}", "OwnerviewController@restore"); //undo delete
	Route::get("/ownerview/{id}/edit", "OwnerviewController@edit");		// edit form owner
	Route::get("/ownerview/{id}/show", "OwnerviewController@show");  //view owner details
	Route::put("/ownerview/{id}", "OwnerviewController@update");	 //save update/changes at the form
	Route::get("/ownerview/create", "EventController@create");	 //create new events form
	// Individual OWNER - all about the views of invitees that owner can see
	// Route::get("/ownerview/inviteelist", "OwnerviewController@inviteelist");

	// ADMIN/USER - all about EVENTS pages
	// out of admin user so non-user can access since they should be able to CRUD events too.
	Route::get("/events", "EventController@index");
	Route::post('/events', 'EventController@store');
	Route::delete("/events/{id}", "EventController@destroy");
	Route::get("/restore/{id}", "EventController@restore");
	Route::get("/events/{id}/edit", "EventController@edit");
	Route::get("/events/{id}/show", "EventController@show");
	Route::put("/events/{id}", "EventController@update");
	Route::get("/events/create", "EventController@create");

	// ADMIN/USER - all about INVITEES pages
	Route::get("/invitees", "InviteeController@index");
	Route::post('/invitees', 'InviteeController@store');
	Route::delete("/invitees/{id}", "InviteeController@destroy");
	Route::get("/restore/{id}", "InviteeController@restore");
	Route::get("/invitees/{id}/edit", "InviteeController@edit");
	Route::get("/invitees/{id}/show", "InviteeController@show");
	Route::put("/invitees/{id}", "InviteeController@update");
	Route::get("/invitees/create", "InviteeController@create");


	// Route::get('/events', 'EventController@index')->middleware("auth");
	//if ADMIN and naka-login
	Route::group(["middleware" => ["isAdmin", "auth"]], function()
	{

		// Route:get("/events", "EventController@index");
		Route::get("/users", "UserController@users");

		Route::get("/users", "UserController@users");

		// ADMIN - all about EVENTS page

		// ADMIN - page about list all event OWNERS
		Route::post('/owners', 'OwnerController@store');
		Route::get("/owners", "OwnerController@index");
		Route::delete("/owners/{id}", "OwnerController@destroy");
		Route::get("/owners/restore/{id}", "OwnerController@restore");
		Route::get("/owners/{id}/edit", "OwnerController@edit");
		Route::get("/owners/{id}/show", "OwnerController@show");
		Route::put("/owners/{id}", "OwnerController@update");
		Route::get("/owners/create", "OwnerController@create");

		// Route::get("users/{id}/approve", "UserController@approve")->name("approve.user");
	});

		// // Individual OWNER - all about OwnerVIEW page
		// Route::post('/ownerview/{id}', 'OwnerviewController@store');	//store the crated new owner
		// Route::get("/ownerview/{id}", "OwnerviewController@index");		//view owners list
		// Route::delete("/ownerview/{id}", "OwnerviewController@destroy"); //soft delete owner
		// Route::get("/ownerview/restore/{id}", "OwnerviewController@restore"); //undo delete
		// Route::get("/ownerview/{id}/edit", "OwnerviewController@edit");		// edit form owner
		// Route::get("/ownerview/{id}/show", "OwnerviewController@show");  //view owner details
		// Route::put("/ownerview/{id}", "OwnerviewController@update");	 //save update/changes at the form
		// Route::get("/ownerview/create", "EventController@create");	 //create new events form
		// // Individual OWNER - all about the views of invitees that owner can see
		// // Route::get("/ownerview/inviteelist", "OwnerviewController@inviteelist");

});