<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::insert
        ([
            [   'name' => "Little Ricky's 1st Birthday",
                'occassion' => "Birthday", 
                'place' => '123 St. Barangay ABC, Makati City', 
                'date' => "October 23, 2019",
                'status' => "upcoming",
                'owner_id' => 2
            ],
            [   'name' => "Desi's Elementary Graduation",
                'occassion' => "Graduation", 
                'place' => '234 St. Barangay DEF, Pasay City', 
                'date' => "March 24, 2019", 
                'status' => "upcoming",
                'owner_id' => 3
            ],
            [   'name' => "Vivian's Bridal Shower",
                'occassion' => "Others", 
                'place' => '345 St. Barangay HIJ, Quezon City', 
                'date' => "April 25, 2019",
                'status' => "archived",
                'owner_id' => 4
            ],
            [   'name' => "William's Championship Celebration",
                'occassion' => "Others",
                'place' => '456 St. Barangay HIJ, Pasig City', 
                'date' => "May 26, 2019",
                'status' => "archived",
                'owner_id' => 5
            ],

// ------------------------------
            [   'name' => "Little Lucy's 5st Birthday",
                'occassion' => "Birthday", 
                'place' => '123 St. Barangay ABC, Makati City', 
                'date' => "January 23, 2020",
                'status' => "archived",
                'owner_id' => 2
            ],
            [   'name' => "Lucille's High School Graduation",
                'occassion' => "Graduation", 
                'place' => '234 St. Barangay DEF, Pasay City', 
                'date' => "March 24, 2019", 
                'status' => "archived",
                'owner_id' => 3
            ],
            [   'name' => "Ricardo Family's Christmas Party",
                'occassion' => "Others", 
                'place' => '345 St. Barangay HIJ, Quezon City', 
                'date' => "December 25, 2019",
                'status' => "upcoming",
                'owner_id' => 4
            ],
            [   'name' => "Mert'z Aparment Tenants New Year Celebration",
                'occassion' => "Others",
                'place' => '456 St. Barangay HIJ, Pasig City', 
                'date' => "January 01, 2020",
                'status' => "upcoming",
                'owner_id' => 5
            ]
        ]);
    }
}
