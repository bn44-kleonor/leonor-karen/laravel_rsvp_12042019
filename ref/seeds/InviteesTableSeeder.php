<?php

use Illuminate\Database\Seeder;
use App\Invitee;

class InviteesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invitee::insert([
            [
                'first_name' => 'Juan', 
                'last_name' => 'dela Cruz', 
                'email' => 'juan@gmail.com', 
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1
            ],
            [
                'first_name' => 'Matt', 
                'last_name' => 'dela Cruz', 
                'email' => 'matt@gmail.com', 
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2
            ],
            [
                'first_name' => 'Simon', 
                'last_name' => 'dela Cruz', 
                'email' => 'simon@gmail.com', 
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3
            ],
            [
                'first_name' => 'Gregorio', 
                'last_name' => 'dela Cruz', 
                'email' => 'gregorio@gmail.com', 
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4
            ],
            [
                'first_name' => 'Juan2', 
                'last_name' => 'dela Cruz', 
                'email' => 'juan2@gmail.com', 
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1
            ],
            [
                'first_name' => 'Matt2', 
                'last_name' => 'dela Cruz', 
                'email' => 'matt2@gmail.com', 
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2
            ],
            [
                'first_name' => 'Simon2', 
                'last_name' => 'dela Cruz', 
                'email' => 'simon2@gmail.com', 
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3
            ],
            [
                'first_name' => 'Gregorio2', 
                'last_name' => 'dela Cruz', 
                'email' => 'gregorio2@gmail.com', 
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4
            ],
            [
                'first_name' => 'Juan3', 
                'last_name' => 'dela Cruz', 
                'email' => 'juan3@gmail.com', 
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1
            ],
            [
                'first_name' => 'Matt3', 
                'last_name' => 'dela Cruz', 
                'email' => 'matt3@gmail.com', 
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2
            ],
            [
                'first_name' => 'Simon3', 
                'last_name' => 'dela Cruz3', 
                'email' => 'simon3@gmail.com', 
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3
            ],
            [
                'first_name' => 'Gregorio3', 
                'last_name' => 'dela Cruz', 
                'email' => 'gregorio3@gmail.com', 
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4
            ],
            [
                'first_name' => 'Juan4', 
                'last_name' => 'dela Cruz', 
                'email' => 'juan4@gmail.com', 
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1
            ],
            [
                'first_name' => 'Matt4', 
                'last_name' => 'dela Cruz', 
                'email' => 'matt4@gmail.com', 
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2
            ],
            [
                'first_name' => 'Simon4', 
                'last_name' => 'dela Cruz', 
                'email' => 'simon4@gmail.com', 
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3
            ],
            [
                'first_name' => 'Gregorio4', 
                'last_name' => 'dela Cruz', 
                'email' => 'gregorio4@gmail.com', 
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4
            ],
            [
                'first_name' => 'Juan5', 
                'last_name' => 'dela Cruz', 
                'email' => 'juan5@gmail.com', 
                'contact_number' => 12345,
                'response' => 'yes',
                'event_id' => 1
            ],
            [
                'first_name' => 'Matt5', 
                'last_name' => 'dela Cruz', 
                'email' => 'matt5@gmail.com', 
                'contact_number' => 23456,
                'response' => 'no',
                'event_id' => 2
            ],
            [
                'first_name' => 'Simon5', 
                'last_name' => 'dela Cruz', 
                'email' => 'simon5@gmail.com', 
                'contact_number' => 34567,
                'response' => 'maybe',
                'event_id' => 3
            ],
            [
                'first_name' => 'Gregorio5', 
                'last_name' => 'dela Cruz', 
                'email' => 'gregorio5@gmail.com', 
                'contact_number' => 45678,
                'response' => '',
                'event_id' => 4
            ]
        ]);
    }
}
