<?php

use Illuminate\Database\Seeder;
use App\Owner;

class OwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Owner::insert([
            [
                'first_name' => 'TBA', 
                'last_name' => '', 
                'email' => 'tba@gmail.com',
                'address' => 'tba',
                'user_id' => 1
            ],
            [
                'first_name' => 'Lucy', 
                'last_name' => 'Ricardo', 
                'email' => 'lucy@gmail.com', 
                'address' => '1 St. Barangay A, Bacolod City',
                'user_id' => 2
            ],
            [
                'first_name' => 'Ricky', 
                'last_name' => 'Ricardo', 
                'email' => 'ricky@gmail.com', 
                'address' => '2 St. Barangay B, Iloilo City',
                'user_id' => 3
            ],
            [
                'first_name' => 'Ethel', 
                'last_name' => 'Mertz', 
                'email' => 'ethel@gmail.com', 
                'address' => '3 St. Barangay C, Davao City',
                'user_id' => 4
            ],
            [
                'first_name' => 'Fred', 
                'last_name' => 'Mertz', 
                'email' => 'desi@gmail.com', 
                'address' => '4 St. Barangay D, Baguio City',
                'user_id' => 5
            ]
        ]);
    }
}
