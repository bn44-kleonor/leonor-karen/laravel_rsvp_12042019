<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'username' => 'admin', 
                'role' => 'admin', 
                'email' => 'admin@gmail.com', 
                'password' => Hash::make('admin123')
            ],
            [
                'username' => 'lucy', 
                'role' => 'user', 
                'email' => 'lucy@gmail.com', 
                'password' => Hash::make('lucy123')
            ],
            [
                'username' => 'ricky', 
                'role' => 'user', 
                'email' => 'ricky@gmail.com', 
                'password' => Hash::make('ricky123')
            ],
            [
                'username' => 'ethel', 
                'role' => 'user', 
                'email' => 'ethel@gmail.com', 
                'password' => Hash::make('ethel123')
            ],
            [
                'username' => 'fred', 
                'role' => 'user', 
                'email' => 'fred@gmail.com', 
                'password' => Hash::make('fred123')
            ]
        ]);
    }
}
