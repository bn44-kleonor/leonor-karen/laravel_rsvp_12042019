<?php

use Illuminate\Database\Seeder;

class OwnersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('owners')->delete();
        
        \DB::table('owners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'TBA',
                'last_name' => 'test',
                'email' => 'tba@gmail.com',
                'address' => 'tba',
                'user_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'Lucy',
                'last_name' => 'Ricardo',
                'email' => 'lucy@gmail.com',
                'address' => '1 St. Barangay A, Bacolod City',
                'user_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'first_name' => 'Ricky',
                'last_name' => 'Ricardo',
                'email' => 'ricky@gmail.com',
                'address' => '2 St. Barangay B, Iloilo City',
                'user_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'first_name' => 'Ethel',
                'last_name' => 'Mertz',
                'email' => 'ethel@gmail.com',
                'address' => '3 St. Barangay C, Davao City',
                'user_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'first_name' => 'Fred',
                'last_name' => 'Mertz',
                'email' => 'desi@gmail.com',
                'address' => '4 St. Barangay D, Baguio City',
                'user_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}