<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => NULL,
                'last_name' => NULL,
                'username' => 'admin',
                'address' => NULL,
                'contact_number' => NULL,
                'role' => 'admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$6Hi6aKhW7Rpr30QGQhzLTOOHUEcQpqNzfC3eb7KA7p/D.olV5AmEG',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => NULL,
                'last_name' => NULL,
                'username' => 'lucy',
                'address' => NULL,
                'contact_number' => NULL,
                'role' => 'user',
                'email' => 'lucy@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vYT3DQcFOBPp72SAj9UA5O.t6Dj4Kq9IbiXe/jOT2jhTIpT156aXK',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'first_name' => NULL,
                'last_name' => NULL,
                'username' => 'ricky',
                'address' => NULL,
                'contact_number' => NULL,
                'role' => 'user',
                'email' => 'ricky@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vgqgLOWYzXsOhKS4DDjqJOH0qCUAokZSS6vjQOgHIyJBlSvOZoV4m',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'first_name' => NULL,
                'last_name' => NULL,
                'username' => 'ethel',
                'address' => NULL,
                'contact_number' => NULL,
                'role' => 'user',
                'email' => 'ethel@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$E4pqNoOx7m0QxEyE/FrrUu5UY7pSbTmpmT9/6BvE8/IyU611YvVxS',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'first_name' => NULL,
                'last_name' => NULL,
                'username' => 'fred',
                'address' => NULL,
                'contact_number' => NULL,
                'role' => 'user',
                'email' => 'fred@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$h1HbhSmTAS.csDDu5TtpwORPwHPXlWI85BXGCJHznqd/KNA6LwTne',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}