<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('occassion');
            $table->string('place');
            $table->string('date');
            $table->enum('status', ['upcoming', 'archived'])->default('upcoming');
            $table->unsignedBigInteger('owner_id');
            $table->nullableTimeStamps();
            
            $table->foreign('owner_id')
            ->references('id')
            ->on('owners')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

