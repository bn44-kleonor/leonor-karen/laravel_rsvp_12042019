<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);
        //dd(Auth());
        // dd(Auth::check());
        
        //if hindi naka log in ang user, babalik sa same page.
        // same to: if(!Auth::check()){
        // if(!Auth::check() || Auth::user()->role == "user"){
        if(Auth::user()->role == "user"){    
            // dd("di ka logged in");
            return redirect()->back();
        }

        //go to the Admin pages that you want to access
        return $next($request);
    }
}
