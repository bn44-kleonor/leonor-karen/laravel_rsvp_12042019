<?php

namespace App\Http\Controllers;

use App\Invitee;
use Illuminate\Http\Request;

class InviteeController extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Invite a visitor";
        return view("invitees.create")->with('title', $title);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        dd('test');
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'contact_number' => 'required',
            'response' => 'required',
            'event_id' => 'required'
        ]);
        // dd('test');
        //create new post
        $invitee = new Invitee;
        $invitee->first_name = $request->input('first_name');
        $invitee->last_name = $request->input('last_name');
        $invitee->email = $request->input('email');
        $invitee->contact_number = $request->input('contact_number');
        $invitee->response = $request->input('response');
        $invitee->event_id = $request->input('event_id');
        $invitee->save();
        $id = $invitee->id;

        return redirect("/ownerview/inviteelist")->with('success', "$invitee->first_name $invitee->last_name was successfully added!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invitee  $invitee
     * @return \Illuminate\Http\Response
     */
    public function show(Invitee $invitee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invitee  $invitee
     * @return \Illuminate\Http\Response
     */
    public function edit(Invitee $invitee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invitee  $invitee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invitee $invitee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invitee  $invitee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitee $invitee)
    {
        //
    }
}
