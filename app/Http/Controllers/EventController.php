<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Event;
use App\Owner;

class EventController extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Events list";
        // $events = Event::orderBy("name", "desc")->paginate(10);
        
        // $events = DB::table('events')
        //     ->join('owners', 'events.owner_id', '=', 'owners.id')
        //     ->select('events.*', 'owners.first_name as ownerfname', 'owners.last_name as ownerlname')
        //     ->orderBy("name", "desc")
        //     ->get();
            // ->paginate(10);

        // $users = DB::table('users')
        // ->join('contacts', 'users.id', '=', 'contacts.user_id')
        // ->join('orders', 'users.id', '=', 'orders.user_id')
        // ->select('users.*', 'contacts.phone', 'orders.price')
        // ->get();

        $events = Event::orderBy("name", "desc")
            ->paginate(10);

        // dd($events);

        return view("events.index", compact("title", "events"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create Event";
        return view("events.create")->with('title', $title);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd('test');
        $this->validate($request, [
            'name' => 'required',
            'occassion' => 'required',
            'place' => 'required',
            'date' => 'required',
            'owner_id' => 'required'
        ]);
        // dd('test');
        //create new post
        $event = new Event;
        $event->name = $request->input('name');
        $event->occassion = $request->input('occassion');
        $event->place = $request->input('place');
        $event->date = $request->input('date');
        $event->owner_id = $request->input('owner_id');
        $event->save();
        $id = $event->id;

        return redirect("/events")->with('success', "$event->name was successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $title = "View Event Page";
        $event = DB::table('events')
            ->join('owners', 'events.owner_id', '=', 'owners.id')
            ->select('events.*', 'owners.first_name as ownerfname', 'owners.last_name as ownerlname')
            ->where('events.id', '=', $id)
            ->first();
        
        // dd($event);
        return view("events.show", compact("title", "event"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $title = "Edit Event Page";
        // $event = Event::find($id);
        // return view("events.edit", compact("title", "event"));

        $title = "Edit Event Page";
        $event = DB::table('events')
            ->join('owners', 'events.owner_id', '=', 'owners.id')
            ->select('events.*', 'owners.first_name as ownerfname', 'owners.last_name as ownerlname')
            ->where('events.id', '=', $id)
            ->first();
        
        // dd($event);
        return view("events.edit", compact("title", "event"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd("test");
        $this->validate($request, [
            'name' => 'required',
            "occassion" => "required",
            "place" => "required",
            "date" =>  "required"
            // "first_name" =>  "required",
            // "last_name" =>  "required"
            ]);
        // dd("test");
        $event = Event::find($id);
        $event->name = $request->input("name");
        $event->occassion = $request->input("occassion");
        $event->place = $request->input("place");
        $event->date = $request->input("date");
        // $owner->first_name = $request->input("first_name");
        // $owner->last_name = $request->input("last_name");


        $event->save();
        return redirect()->back()->with("success", "Changes has been saved!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event_name = $event->name;
        $event->delete();
        // return redirect()->back()->with("success", "$product_name has been deleted!");
        return redirect("/events")
        ->with("success", "$event_name has been deleted!")
        ->with("undo_url","/restore/$id");
    }

    public function restore($id)
    {
        //get softdeleted products

        $event = Event::onlyTrashed()->where("id", $id)->first();  //get returns a collection
        // dd($event_name);

        $event->restore();
        return back()->with("success", "$event->name has been restored!");
        // return redirect("/events")
        // ->with("success", "$event->name has been restored!");
    }
}
