<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class UserController extends Controller
{
	public function()
	{
	    $users = User::orderBy("username")->paginate(20);
	    return view("admin.users", compact("users"));
	}

	// public function approve($id) 
	// {
	// 	$user = User::find($id);
	// 	$user->approved_at = now();
	// 	$user->save();

	// 	Session::flash("success", "$user->username has been successfully approved!");
	// 	return redirect("/users");
	// }
}