<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;

class CartController extends Controller
{
    public function addToCart(Request $request, $id) 
    {
    	//get quantity from form 
    	$quantity = $request->quantity;
    	// dd($quantity);

    	//check if there is a cart session
    	if(Session::has("cart"))
    	{
    		//get data from cart session
    		$cart = Session::get("cart");
    	}
    	//otherwise
    	else
    	{
    		//initialize cart as an empty array
    		$cart = [];
    	}

    	//check if added product is in cart session
    	if(isset($cart[$id]))
    	{
    		//add to existing quantity through array push
    		$cart[$id] += $quantity;
    	}
    	else
    	{
    		//assign quantity to product (so they'll be "key-value pair")
    		$cart[$id] = $quantity;
    	}

    	//save changes to cart
    	Session::put("cart", $cart);

    	$product = Product::find($id);

    	//redirect
    	// return redirect()->back()->with("success", "$product->name has been added to cart!");
    	
    	$count = collect(Session::get("cart"))->sum();
    	return $count;

    }

    public function updateQuantity(Request $request, $id)
    {
        // dd("test");
        $cart = Session::get("cart");

        // 'request' from script.js (body: new FormData(form))
        $cart[$id] = $request->new_quantity;
        Session::put("cart", $cart);

        $cart_products = [];

        //set total 
        $total = 0;

        //dd(Session);
            $cart = Session::get("cart");
            foreach($cart as $id => $quantity)  //"quantity" nasa 'Session'
            {
                //find the product via its id
                $product = Product::find($id);

                //add quantity attribute to product
                $product->quantity = $quantity;

                //add subtotal attribute to product
                $product->subtotal = $product->price * $quantity;

                //get total
                $total += $product->subtotal;

                //array push product to cart_products variable
                $cart_products[] = $product;
            }

        return view("partials.cart_items", compact("cart_products", "total"))->render();
    }

    public function destroyCartProduct(Request $request, $id)
    {
        // dd("test");
        Session::forget("cart.$id");

        $cart_products = [];

        //set total 
        $total = 0;

        //dd(Session);
            $cart = Session::get("cart");
            foreach($cart as $id => $quantity)  //"quantity" nasa 'Session'
            {
                //find the product via its id
                $product = Product::find($id);

                //add quantity attribute to product
                $product->quantity = $quantity;

                //add subtotal attribute to product
                $product->subtotal = $product->price * $quantity;

                //get total
                $total += $product->subtotal;

                //array push product to cart_products variable
                $cart_products[] = $product;
            }

        return view("partials.cart_items", compact("cart_products", "total"))->render();
    }


}
